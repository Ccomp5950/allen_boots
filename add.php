<?php
/**
 * Created by Charles.
 * User: charles
 * Date: 6/21/14
 * Time: 4:59 PM
 */
$pageRequiresAdmin = 0;
$pageRequiresLogin = 1;
$add_message = "";
require 'header_common.php';
$ArrayMat = build_materialArray();
$ArraySMat = build_submatArray();
$ArrayHeel = build_heelArray();
$ArrayHeight = build_heightArray();
if ($_POST) {
    $error = 0;
    list($add_message, $error) = check_add();


    if (!$error) {
        $mysqli = DB::cxn();
        $query = "insert into boots (material, submaterial, heel, size, color, description, height, inventoryID, retailPrice, wholesalePrice, type) values (?,?,?,?,?,?,?,?,?,?,?);";
        $stmt = $mysqli->prepare($query);
        if ($stmt === false) {
            trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
            die();
        }
        $stmt->bind_param("iiiissiiddi", $_POST['material'], $_POST['submat'], $_POST['heel'], $_POST['size'], $_POST['color'],
            $_POST['desc'], $_POST['height'], $_POST['invID'], $_POST['retail'], $_POST['wholesale'], $_POST['inputType']);
        if ($stmt->execute()) {
            $add_message = <<<HEREGOOD
            <div class="bs-component">
                  <div class="alert alert-dismissable alert-success">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Boot Added to database.</strong>
                  </div>
                </div>
HEREGOOD;
        } else {
            trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
            die();
        }
    }


}

$MatSelect = build_selectOptions($ArrayMat, "material", 0, 0, 0, 0, "onchange='updateSubMats()'");
$SMatSelect = build_selectOptions($ArraySMat, "submat", 0, 1, 0);
$HeelSelect = build_selectOptions($ArrayHeel, "heel", 0, 0, 1);
$HeightSelect = build_selectOptions($ArrayHeight, "height", 0, 1);


echo <<<HERETEXT


<div class="container">
    <div class="row">
        $add_message
        <h2>Add inventory</h2>
    </div>
    <div class="row">
        <form class="form-horizontal" action="./add.php" method="post">

        <div class="col-lg-2 col-xs-1">
        </div>
        <div class="col-lg-8 col-xs-10">
            <div class="form-group">
                <label class="col-lg-3 control-label">Type</label>
                <div class="col-lg-4">
                    <div class="radio">
                        <label>
                            <input type="radio" name="inputType" id="inputType1" value="1">
                            Boots
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="inputType" id="inputType2" value="2">
                            Shoes
                        </label>
                    </div>
                </div>
                <div class="col-lg-3" id="selectAdvice">
                </br><p class="text-warning">Select boots or shoes first, please</p>
                </div>
            </br>
            </div>

            <div class="form-group">
                <label for="inputMat" class="col-lg-3 control-label">Material</label>
                <div class="col-lg-4">
                    $MatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group" id="submatFG">
                <label for="inputSMat" class="col-lg-3 control-label">Sub-Material</label>
                <div class="col-lg-4">
                    $SMatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group">
                <label for="inputHeel" class="col-lg-3 control-label">Heel</label>
                <div class="col-lg-4">
                    $HeelSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group" id="heightSelectFG">
                <label for="inputHeight" class="col-lg-3 control-label">Height</label>
                <div class="col-lg-4">
                    $HeightSelect
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label">Size</label>
                <div class="col-lg-4">
                    <input type="text" onchange='updateSize()' name="size" class="form-control" placeholder="37" required>
                </div>
                <div class="col-lg-5 hidden-xs" id="sizeChange">
                <p class="text-warning">You can use the [TAB] key on your keyboard to quickly jump to the next input.</p>
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label">Color</label>
                <div class="col-lg-4">
                    <input type="text" name="color" class="form-control" placeholder="Red" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDesc" class="col-lg-3 control-label">Description</label>
                <div class="col-lg-4">
                    <input type="text" name="desc" class="form-control" placeholder="Description">
                </div>
            </div>
            <div class="form-group">
                <label for="inputInvID" class="col-lg-3 control-label">Inventory ID</label>
                <div class="col-lg-4">
                    <input type="text" name="invID" class="form-control" placeholder="Inventory ID" required>
                </div>
            </div>
            <div class="form-group">
                <label for="wholesale" class="col-lg-3 control-label">Wholesale price</label>
                <div class="col-lg-4">
                    <input type="text" name="wholesale" class="form-control" placeholder="120.00" required>
                </div>
            </div>
            <div class="form-group">
                <label for="retail" class="col-lg-3 control-label">Retail price</label>
                <div class="col-lg-4">
                    <input type="text" name="retail" class="form-control" placeholder="240.00" required>
                </div>
            </div>



        </div>


        <div class="col-lg-2 col-xs-1">
        <!-- Right -->

        </div>

    </div> <!-- /row -->

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
        <button class="btn btn-success btn-lg btn-block" type="submit">ADD BOOT</button>
        </div>
        <div class="col-lg-2">
        <!-- Right -->
    </form>
    </div> <!-- /row -->
</div> <!-- /container -->

HERETEXT;


require 'footer_common.php'
?>

<!-- Start hide stuffs script -->
<script>

    function updateSubMats() {
        var selectMat = document.getElementById('material').value;
        if (selectMat == 1) {
            $("#submatFG").removeClass("hidden");
            //$("#heightSelectFG").removeClass("hidden");
            document.getElementById('submat').value = 1;

        } else {
            $("#submatFG").addClass("hidden");

            document.getElementById('submat').value = 0;

        }

    }

    $(document).ready(function () {
        var rbBoot = document.getElementById('inputType1');
        var rbShoe = document.getElementById('inputType2');
        var selectHeel = document.getElementById('heel');
        var selectHeight = document.getElementById('height');

        rbBoot.addEventListener('change', function (e) {
            $('.boot_type_1').removeClass("hidden");
            $('.boot_type_2').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").removeClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 1
            selectHeel.value = 1;
        })

        rbShoe.addEventListener('change', function (e) {
            $('.boot_type_2').removeClass("hidden");
            $('.boot_type_1').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").addClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 0
            selectHeel.value = 6;
        })
    })
</script>

</body>
</html>
