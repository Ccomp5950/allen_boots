<?php
/**
 * Created by Charles.
 * User: charles
 * Date: 6/26/14
 * Time: 2:28 PM
 */

$pageRequiresAdmin = 0;
$pageRequiresLogin = 1;
$add_message = "";
require_once 'config.php';
require_once 'common.php';
require_once 'handle_login.php';
$ArrayMat = build_materialArray();
$ArraySMat = build_submatArray();
$ArrayHeel = build_heelArray();
$ArrayHeight = build_heightArray();
/*
 * $boots[$bootID] = array("ID" => $bootID, "mat" => $bootmat, "smat" => $bootsmat,
            "heel" => $bootheel, "size" => $bootsize, "color" => $bootcolor, "desc" => $bootdesc,
            "height" => $bootheight, "InvID" => $bootInvID, "RPrice" => $bootRPrice, "WPrice" => $bootWPrice);
 */

$boot = array("ID" => 0, "mat" => 0, "smat" => 0,
    "heel" => 0, "size" => 0, "color" => 0, "desc" => "",
    "height" => 0, "InvID" => 0, "RPrice" => 0, "WPrice" => 0);
if ($_POST) {
    if (isset($_POST['edit'])) {
        $boots = build_bootArray($_POST['edit']);
        $boot = $boots[$_POST['edit']];
    }
    if (isset($_POST['edit2'])) {
        list($add_message, $error) = check_add(); //Check for issues / SQL injection attempts
        if (!$error) {
            $mysqli = DB::cxn();
            $query = "update boots set material=?, submaterial=?, heel=?, size=?, color=?, description=?, height=?, inventoryID=?, retailPrice=?, wholesalePrice=?, type=? where id=?;";
            $stmt = $mysqli->prepare($query);

            $stmt->bind_param("iiiissiiddii", $_POST['material'], $_POST['submat'], $_POST['heel'], $_POST['size'], $_POST['color'],
                $_POST['desc'], $_POST['height'], $_POST['invID'], $_POST['retail'], $_POST['wholesale'], $_POST['inputType'], $_POST['edit2']);
            if ($stmt->execute()) {
                header("location: " . Config::cleanPrefix . "/inventory.php?showAll=1");
                die();
            } else {
                trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
                die();
            }

        }
    }


} else {
    header("location: " . Config::cleanPrefix . "/");
    die;
}
$MatSelect = build_selectOptions($ArrayMat, "material", $boot['mat'], 0, 0, "onchange='updateSubMats()'");
$SMatSelect = build_selectOptions($ArraySMat, "submat", $boot['smat'], 1, 0);
$HeelSelect = build_selectOptions($ArrayHeel, "heel", $boot['heel'], 0, 1, $boot['type']);
$HeightSelect = build_selectOptions($ArrayHeight, "height", $boot['height'], 1);

require 'header_common.php';

$smathidden = "";
if (is_null($boot['smat']) || $boot['smat'] == 0)
    $smathidden = " hidden";
$bootselected = "";
$shoeselected = "";

if ($boot['type'] == 1) {
    $bootselected = "checked";
} else {
    $shoeselected = "checked";
}

echo <<<HERETEXT


<div class="container">
    <div class="row">
        $add_message
        <h2>Edit inventory</h2>
    </div>
    <div class="row">
        <form class="form-horizontal" action="./edit.php" method="post">

        <div class="col-lg-2 col-xs-1">
        </div>
        <div class="col-lg-8 col-xs-10">
            <div class="form-group">
                <label class="col-lg-3 control-label">Type</label>
                <div class="col-lg-4">
                    <div class="radio">
                        <label>
                            <input type="radio" name="inputType" id="inputType1" value="1" $bootselected>
                            Boots
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="inputType" id="inputType2" value="2" $shoeselected>
                            Shoes
                        </label>
                    </div>
                </div>
                <div class="col-lg-3" id="selectAdvice">
                </br><p class="text-warning">Select boots or shoes first, please</p>
                </div>
            </br>
            </div>

            <div class="form-group">
                <label for="inputMat" class="col-lg-3 control-label">Material</label>
                <div class="col-lg-4">
                    $MatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group$smathidden" id="submatFG">
                <label for="inputSMat" class="col-lg-3 control-label">Sub-Material</label>
                <div class="col-lg-4">
                    $SMatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group">
                <label for="inputHeel" class="col-lg-3 control-label">Heel</label>
                <div class="col-lg-4">
                    $HeelSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group" id="heightSelectFG">
                <label for="inputHeight" class="col-lg-3 control-label">Height</label>
                <div class="col-lg-4">
                    $HeightSelect
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label">Size</label>
                <div class="col-lg-4">
                    <input type="text" onchange='updateSize()' name="size" class="form-control" placeholder="37" required value='${boot['size']}'>
                </div>
                <div class="col-lg-5 hidden-xs" id="sizeChange">
                <p class="text-warning">You can use the [TAB] key on your keyboard to quickly jump to the next input.</p>
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label">Color</label>
                <div class="col-lg-4">
                    <input type="text" name="color" class="form-control" placeholder="Red" required value='${boot['color']}'>
                </div>
            </div>
            <div class="form-group">
                <label for="inputDesc" class="col-lg-3 control-label">Description</label>
                <div class="col-lg-4">
                    <input type="text" name="desc" class="form-control" placeholder="Description" value='${boot['desc']}'>
                </div>
            </div>
            <div class="form-group">
                <label for="inputInvID" class="col-lg-3 control-label">Inventory ID</label>
                <div class="col-lg-4">
                    <input type="text" name="invID" class="form-control" placeholder="Inventory ID" required value='${boot['InvID']}'>
                </div>
            </div>
            <div class="form-group">
                <label for="wholesale" class="col-lg-3 control-label">Wholesale price</label>
                <div class="col-lg-4">
                    <input type="text" name="wholesale" class="form-control" placeholder="120.00" required value='${boot['WPrice']}'>
                </div>
            </div>
            <div class="form-group">
                <label for="retail" class="col-lg-3 control-label">Retail price</label>
                <div class="col-lg-4">
                    <input type="text" name="retail" class="form-control" placeholder="240.00" required value='${boot['RPrice']}'>
                </div>
            </div>



        </div>


        <div class="col-lg-2 col-xs-1">
        <!-- Right -->

        </div>

    </div> <!-- /row -->

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
        <input type='hidden' name='edit2' value='${boot['ID']}'>
        <button class="btn btn-success btn-md btn-block" type="submit">EDIT BOOT</button>
        </div>
        <div class="col-lg-2">
        <!-- Right -->
    </form>
    </div> <!-- /row -->
</div> <!-- /container -->

HERETEXT;


require 'footer_common.php'
?>

<!-- Start hide stuffs script -->
<script>

    function updateSubMats() {
        var selectMat = document.getElementById('material').value;
        if (selectMat == 1) {
            $("#submatFG").removeClass("hidden");
            $("#heightSelectFG").removeClass("hidden");
            document.getElementById('submat').value = 1;

        } else {
            $("#submatFG").addClass("hidden");

            document.getElementById('submat').value = 0;

        }

    }

    $(document).ready(function () {
        var rbBoot = document.getElementById('inputType1');
        var rbShoe = document.getElementById('inputType2');
        var selectHeel = document.getElementById('heel');
        var selectHeight = document.getElementById('height');

        rbBoot.addEventListener('change', function (e) {
            $('.boot_type_1').removeClass("hidden");
            $('.boot_type_2').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").removeClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 1
            selectHeel.value = 1;
        })

        rbShoe.addEventListener('change', function (e) {
            $('.boot_type_2').removeClass("hidden");
            $('.boot_type_1').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").addClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 0
            selectHeel.value = 6;
        })
    })
</script>

</body>
</html>
