<?php

class Config
{


    const
        projectTitle = "Exotic Stitch",
        windowTitle = "Exotic Stitch Boot Company Inventory Control Panel",

        sessionValidTime = 1440, //Time in minutes the session is valid for.

        salesTax = 0.0825,
        chargeSalesTax = 1,


        /*
         * MySQL information.  This is the database we use to store the inventory.
         */

        mysqlUser = "root",
        mysqlPassword = "450712584",
        mysqlLocation = "localhost", //Default:  localhost
        mysqlPort = 3306, //Default:  3306
        mysqlTable = "boots", //Default:  boots, if you require any prefixes place them here as well.


        /*
         *   HTML path information.  Navigation won't work if this isn't setup properly.
         */
        prefix = "'/boots'", // used for determining what page you are on.
        cleanPrefix = "/boots"; // The prefix your links will need.  Slash at front slash at back.


}