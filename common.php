<?php

/**
 * Created by Charles Compton.
 * Purpose:  Contains the common functions that each page will need.
 * User: charles
 * Date: 6/21/14
 * Time: 8:40 PM
 */
class DB
{
    private static $mysqli;

    private function __construct()
    {
    } //no instantiation

    static function cxn()
    {
        if (!self::$mysqli) {
            self::$mysqli = new mysqli(Config::mysqlLocation, Config::mysqlUser, Config::mysqlPassword, Config::mysqlTable, Config::mysqlPort);
        }
        return self::$mysqli;
    }
}

function isValidMd5($md5 = '')
{
    return preg_match('/^[a-f0-9]{32}$/', $md5);
}

function calendar($suffixf = NULL, $dayf = NULL, $monthf = NULL, $yearf = NULL)
{
    if ($dayf == NULL) $dayf = date("j");
    if ($monthf == NULL) $monthf = date("n");
    if ($yearf == NULL) $yearf = date("Y");

    echo "<select Name='day$suffixf'>";
    For ($j = 1; $j != 32; $j++) {
        $selected = ($j == $dayf ? "selected" : "");
        echo "<option $selected value='$j'>$j</option>";
    }

    echo "</select>&nbsp;<select name='month$suffixf'>";
    $months = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.');
    For ($j = 1; $j != 13; $j++) {
        $selected = ($j == $monthf ? "selected" : "");
        echo "<option $selected value='$j'>$months[$j]</option>";
    }
    echo "</select>&nbsp;<select name='year$suffixf'>";
    $curyear = date("Y");
    For ($j = $curyear; $j != $curyear + 3; $j++) {
        $selected = ($j == $yearf ? "selected" : "");
        echo "<option $selected value='$j'>$j</option>";
    }
    echo "</select>&nbsp;";
}

function build_heelArray($include_all = 0)
{
    $mysqli = DB::cxn();
    $where = "where active = 1";
    if ($include_all == 1) $where = "";
    $stmt = $mysqli->prepare("select id, heel, type from heel $where order by id;");
    $stmt->execute();
    $stmt->store_result();
    $heelID = "";
    $heel = "";
    $heelType = "";
    $stmt->bind_result($heelID, $heel, $heelType);
    $heels = array();
    while ($stmt->fetch()) {
        $heels[$heelID] = array("heel" => $heel, "type" => $heelType, "id" => $heelID);
    }
    return $heels;
}

function build_materialArray($include_all = 0)
{
    $mysqli = DB::cxn();
    $where = "where active = 1";
    if ($include_all == 1) $where = "";
    $stmt = $mysqli->prepare("select id, material, submaterials from material $where order by id;");
    $stmt->execute();
    $stmt->store_result();
    $matID = "";
    $mat = "";
    $submat = "";
    $stmt->bind_result($matID, $mat, $submat);
    $materials = array();
    while ($stmt->fetch()) {
        $materials[$matID] = array("material" => $mat, "submat" => $submat, "id" => $matID);
    }
    $stmt->close();
    return $materials;
}

function build_submatArray($include_all = 0)
{
    $mysqli = DB::cxn();
    $where = "where active = 1";
    if ($include_all == 1) $where = "";
    $stmt = $mysqli->prepare("select id, name from submaterial $where order by id;");
    $stmt->execute();
    $stmt->store_result();
    $smatID = "";
    $smat = "";
    $stmt->bind_result($smatID, $smat);
    $smaterials = array();
    while ($stmt->fetch()) {
        $smaterials[$smatID] = array("submat" => $smat, "id" => $smatID);
    }
    $stmt->close();
    return $smaterials;
}

function build_heightArray($include_all = 0)
{
    $mysqli = DB::cxn();
    $where = "where active = 1";
    if ($include_all == 1) $where = "";
    $query = "select id, height from height $where order by id;";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
        die();
    }
    $stmt->execute();
    $stmt->store_result();
    $heightID = "";
    $height = "";
    $stmt->bind_result($heightID, $height);
    $heights = array();
    while ($stmt->fetch()) {
        $heights[$heightID] = array("height" => $height, "id" => $heightID);
    }
    return $heights;
}

function build_userArray($include_all = 0, $userid = 0)
{
    $mysqli = DB::cxn();
    $where = "where active = 1";
    if ($userid) {
        $where .= " AND id = $userid";
    }
    if ($include_all == 1) $where = "";
    $query = "select id, user, adminFlag, active from users $where order by id;";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
        die();
    }
    $stmt->execute();
    $stmt->store_result();
    $userID = "";
    $username = "";
    $adminFlag = 1;
    $active = 1;
    $stmt->bind_result($userID, $username, $adminFlag, $active);
    $users = array();
    while ($stmt->fetch()) {
        $users[$userID] = array("UID" => $userID, "username" => $username, "admin" => $adminFlag, "active" => $active);
    }
    return $users;
}

function build_bootArray($id = 0, $type = 0, $mat = 0, $submat = 0, $heel = 0, $size = 0, $height = 0)
{
    $mysqli = DB::cxn();
    $where = "where status = 1";
    if ($type && is_numeric($type)) // We can't use prepared statements that well here.
    { // So instead we do some checks to make sure the
        $where .= " and type = $type"; // input is at least numeric and not something unexpected
    } // or attempts to SQL inject.
    if ($mat && is_numeric($mat)) {
        $where .= " and material = $mat";
    }
    if ($submat && is_numeric($submat)) {
        $where .= " and submaterial = $submat";
    }
    if ($heel && is_numeric($heel)) {
        $where .= " and heel = $heel";
    }
    if ($size && is_numeric($size)) {
        $where .= " and size = $size";
    }
    if ($id && is_numeric($id)) {
        $where .= " and id = $id";
    }
    if ($height && is_numeric($height)) {
        $where .= " and id = $height";
    }
    $query = "select id, material, submaterial, heel, size, color, description, height, inventoryID, retailPrice, wholesalePrice, type from boots $where order by id;";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
        die();
    }
    $stmt->execute();
    $stmt->store_result();
    $bootID = "";
    $bootmat = 0;
    $bootsmat = 0;
    $bootheel = 0;
    $bootsize = 0;
    $bootcolor = "";
    $bootdesc = "";
    $bootheight = "";
    $bootInvID = 0;
    $bootRPrice = 19.99;
    $bootWPrice = 9.99;
    $bootType = 1;
    $stmt->bind_result($bootID, $bootmat, $bootsmat, $bootheel, $bootsize, $bootcolor, $bootdesc, $bootheight, $bootInvID, $bootRPrice, $bootWPrice, $bootType);
    $boots = array();
    while ($stmt->fetch()) {
        $boots[$bootID] = array("ID" => $bootID, "mat" => $bootmat, "smat" => $bootsmat,
            "heel" => $bootheel, "size" => $bootsize, "color" => $bootcolor, "desc" => $bootdesc,
            "height" => $bootheight, "InvID" => $bootInvID, "RPrice" => $bootRPrice, "WPrice" => $bootWPrice, "type" => $bootType);
    }
    $stmt->close();
    return $boots;
}

function build_bootsTable($boots = array(), $showWholesale = 0)
{
    global $ArrayMat, $ArraySMat, $ArrayHeel, $ArrayHeight;

    $wholesaleRow = "";
    if ($showWholesale) {
        $wholesaleRow = "<th>Wholesale</th>";
    }
    $table = <<<HERETABLE
        <table class="table table-striped table-responsive">
            <thead>
                <tr class = "td-lg">
                    <th>Type</th>
                    <th>Material</th>
                    <th>Sub&nbsp;Mat</th>
                    <th>Heel</th>
                    <th>Size</th>
                    <th>Color</th>
                    <th>Height</th>
                    <th>Price</th>
                    $wholesaleRow
                    <th>Description</th>
                    <th>InventoryID</th>
                    <th colspan="2"><center>Actions</center></th>

                </tr>
            </thead>
        <tbody>
HERETABLE;
    $wholesaleRow = "";
    $btype = array(1 => "Boots", 2 => "Shoes");
    foreach ($boots as $boot) {
        if ($showWholesale) {
            $wholesaleRow = "<td>{$boot['WPrice']}</td>";
        }
        $material = ucwords($ArrayMat[$boot['mat']]['material']);
        if (is_null($boot['smat']) || $boot['smat'] == 0) {
            $smaterial = "N/A";
        } else {
            $smaterial = ucwords($ArraySMat[$boot['smat']]['submat']);
        }
        $heel = ucwords($ArrayHeel[$boot['heel']]['heel']);
        if (is_null($boot['height']) || $boot['height'] == 0) {
            $height = "N/A";
        } else {
            $height = ucwords($ArrayHeight[$boot['height']]['height']);
        }
        $type = $btype[$boot['type']];
        $sellbutton = "<form action='./sell.php' method='post'><input type='hidden' name='sell' value='" . $boot['ID'] . "'><button class='btn btn-primary  btn-block' type='submit'>&nbsp;&nbsp;Sell&nbsp;&nbsp;</button></form>";
        $editbutton = "<form action='./edit.php' method='post'><input type='hidden' name='edit' value='" . $boot['ID'] . "'><button class='btn btn-warning  btn-block' type='submit'>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button></form>";


        $table .= <<<HERETABLE
            <tr class = "td-lg">
                <td >$type</td>
                <td >$material</td>
                <td >$smaterial</td>
                <td >$heel</td>
                <td >{$boot['size']}</td>
                <td class="bootdesc">{$boot['color']}</td>
                <td >$height</td>
                <td >\${$boot['RPrice']}</td>
                $wholesaleRow
                <td class=" bootdesc">{$boot['desc']}</td>
                <td>{$boot['InvID']}</td>
                <td>$sellbutton</td>
                <td>$editbutton</td>
            </tr>
HERETABLE;
    }
    $table .= "</tbody>
</table> ";
    return $table;
}

function build_userTable($users = array())
{

    $table = <<<HERETABLE
    <table class="table table-striped table-responsive">
            <thead>
                <tr class = "td-lg">
                    <th>Username</th>
                    <th>Admin</th>
                    <th colspan="3"><center>Actions</center></th>
                </tr>
            </thead>
        <tbody>
HERETABLE;
    foreach ($users as $user) {
        $disabled = "";
        if ($user['UID'] == 1 && $user['username'] == "support") {
            $disabled = "disabled";
        }
        $resetPWbutton = "<form action='./adminP.php' method='post'><input type='hidden' name='reset' value='" . $user['UID'] . "'><button class='btn btn-info  btn-block $disabled' type='submit'>&nbsp;&nbsp;Reset PW&nbsp;&nbsp;</button></form>";
        $editbutton = "<form action='./adminP.php' method='post'><input type='hidden' name='edit' value='" . $user['UID'] . "'><button class='btn btn-success  btn-block $disabled' type='submit'>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button></form>";
        $deletebutton = "<form action='./adminP.php' method='post'><input type='hidden' name='delete' value='" . $user['UID'] . "'><button class='btn btn-danger  btn-block $disabled' type='submit'>&nbsp;&nbsp;Delete&nbsp;&nbsp;</button></form>";
        $admin = "<span class='text-primary'>No</span>";
        if ($user['admin']) {
            $admin = "<span class='text-success'>Yes</span>";
        }
        $table .= <<<HERETABLE
            <tr class = "td-lg">
                <td >${user['username']}</td>
                <td >$admin</td>
                <td > $resetPWbutton</td>
                <td > $editbutton</td>
                <td > $deletebutton</td>
            </tr>
HERETABLE;

    }
    $table .= "</tbody> </table> ";
    return $table;
}

function build_selectOptions($array = array(), $key = "", $default = 0, $na = 0, $typecheck = 0, $type = 0, $js = "", $hidena = 1)
{

    $result = "<select $js class=\"form-control\" name=\"$key\" id=\"$key\">\n";
    if ($typecheck && $type == 0) {
        $result .= "<option class='selectBlank' value='0' id='${key}_0'>Select</option>";
    } elseif ($na) {
        if ($hidena) {
            $result .= "<option class='hidden' value='0' id='${key}_0'>N/A</option>";
        } else {
            $result .= "<option selected='selected' value='0' id='${key}_0'>N/A</option>";
        }
    }
    $first = 0;
    if (!$default && $hidena) $first = 1;
    foreach ($array as $i => $j) {
        $class = "";
        $selected = "";
        if ($first || ($i == $default && $default != 0)) {
            $selected = "selected ='selected'";
        }
        $first = 0;
        if ($typecheck) {
            $class = "class = 'boot_type_${j['type']}";
            if ($type != 0 && $type == $j['type']) {
                $class .= " hidden";
            }
            $class .= "'";
        }
        $result .= "<option $class value='$i' id='${key}_${i}' $selected>" . ucwords($j[$key]) . "</option>";
    }
    $result .= "</select>";
    return $result;
}

function build_HeelSelectOptions($array = array(), $key = "")
{

    $result = "<select class=\"form-control\" name=\"$key\" id=\"$key\">\n";
    $result .= "<option value='0' selected='selected'>N/A</option>\n";
    $result .= "<option value='0'>BOOT HEELS</option>\n";
    $result .= "<option value='0'>-----------</option>\n";

    $typecheck = 1;
    foreach ($array as $i => $j) {
        if ($typecheck && $j['type'] == 2) {
            $result .= "<option value='0'>&nbsp;</option>\n";
            $result .= "<option value='0'>SHOE HEELS</option>\n";
            $result .= "<option value='0'>-----------</option>\n";
            $typecheck = 0;
        }
        $result .= "<option value='$i' id='${key}_${i}' >" . ucwords($j[$key]) . "</option>";
    }
    $result .= "</select>";
    return $result;
}

function add_error($type = "")
{
    $result = "";
    if ($type == "") {
        return $result;
    }
    $result = <<<HERETEXT
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Invalid value for $type, entry was not added.
                </div>
HERETEXT;
    return $result;
}

function check_add()
{
    $add_message = "";
    $error = 0;
    if (!isset($_POST['inputType']) || !is_numeric($_POST['inputType'])) {
        $add_message .= add_error("Type");
        $error++;
    }
    if (!isset($_POST['material']) || !is_numeric($_POST['material'])) {
        $add_message .= add_error("Material");
        $error++;
    }
    if (!isset($_POST['submat']) || !is_numeric($_POST['submat'])) {
        $add_message .= add_error("Sub-Material");
        $error++;
    }
    if (!isset($_POST['heel']) || !is_numeric($_POST['heel'])) {
        $add_message .= add_error("Heel Type");
        $error++;
    }
    if (!isset($_POST['height']) || !is_numeric($_POST['height'])) {
        $add_message .= add_error("Boot Height");
        $error++;
    }
    if (!isset($_POST['wholesale']) || !is_numeric($_POST['wholesale'])) {
        $add_message .= add_error("Wholesale Price");
        $error++;
    }
    if (!isset($_POST['retail']) || !is_numeric($_POST['retail'])) {
        $add_message .= add_error("Retail Price");
        $error++;
    }
    return array($add_message, $error);
}

function check_sell()
{
    $add_message = "";
    $error = 0;
    if (!isset($_POST['retail']) || !is_numeric($_POST['retail'])) {
        $add_message .= add_error("Retail Price");
        $error++;
    }
    return array($add_message, $error);
}

function check_search()
{
    $add_message = "";
    $error = 0;
    if (!isset($_POST['material']) || !is_numeric($_POST['material'])) {
        $add_message .= add_error("Material");
        $error++;
    }
    if (!isset($_POST['submat']) || !is_numeric($_POST['submat'])) {
        $add_message .= add_error("SubMaterial");
        $error++;
    }
    if (!isset($_POST['heel']) || !is_numeric($_POST['heel'])) {
        $add_message .= add_error("Heel Type");
        $error++;
    }
    if (!isset($_POST['height']) || !is_numeric($_POST['height'])) {
        $add_message .= add_error("Boot Height");
        $error++;
    }
    if (!isset($_POST['size']) || !is_numeric($_POST['size'])) {
        $add_message .= add_error("size");
        $error++;
    }
    return array($add_message, $error);
}

function build_salesReport($interval = "")
{
    $mysqli = DB::cxn();
    $query = "select id, retailPrice, wholesalePrice, salestax, totalsale, UNIX_TIMESTAMP(?) as timestamp from boots where status = 2 AND datesold < (?)";
    $stmt = $mysqli->prepare($query);
    if ($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
        die();
    }
    $stmt->bind_param("ss", $interval, $interval);
    if ($stmt === false) {
        trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
        die();
    }
    $stmt->execute();
    $stmt->store_result();
    $bootID = "";
    $retailPrice = "";
    $wholesalePrice = 1;
    $salestax = 1;
    $totalsale = 0;
    $timestamp = "";
    $stmt->bind_result($bootID, $retailPrice, $wholesalePrice, $salestax, $totalsale, $timestamp);
    $rt = 0;
    $wt = 0;
    $st = 0;
    $tt = 0;
    $count = 0;
    if (!$stmt->num_rows) {
        $boots = array("retail" => 0, "wholesale" => 0, "salestax" => 0, "total" => 0, "timestamp" => 0);
        return $boots;
    }
    while ($stmt->fetch()) {
        $count++;
        $rt += $retailPrice;
        $wt += $wholesalePrice;
        $st += $salestax;
        $tt += $totalsale;
        //$boots[$bootID] = array("retail" => $retailPrice, "wholesale" => $wholesalePrice, "salestax" => $salestax, "total" => $totalsale);
    }
    $boots = array("retail" => $rt, "wholesale" => $wt, "salestax" => $st, "total" => $tt, "timestamp" => $timestamp, "bootsold" => $count);
    return $boots;
}