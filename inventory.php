<?php
/**
 * Created by Charles.
 * User: charles
 * Date: 6/21/14
 * Time: 4:59 PM
 *   View 1:  Options of how to display your inventory.
 *   View 2:  Display inventory limited to solution given in view 1
 */
$pageRequiresAdmin = 0;
$pageRequiresLogin = 0;
require 'header_common.php';
$ArrayMat = build_materialArray();
$ArraySMat = build_submatArray();
$ArrayHeel = build_heelArray();
$ArrayHeight = build_heightArray();
$MatSelect = build_selectOptions($ArrayMat, "material", 0, 1, 0, 0, "onchange='updateSubMats()'", 0);
$SMatSelect = build_selectOptions($ArraySMat, "submat", 0, 1, 0, 0, "", 0);
$HeelSelect = build_HeelSelectOptions($ArrayHeel, "heel");
$HeightSelect = build_selectOptions($ArrayHeight, "height", 0, 1, 0, 0, "", 0);

//function build_selectOptions($array = array(), $key = "",$default=0, $na = 0, $typecheck = 0,$type = 0, $js ="")


$view = 1;
if (isset($_GET['showAll'])) {
    $_POST['showAll'] = 1;
}

if ($_POST) {
    $view = 2;
    if (isset($_POST['showAll'])) {
        $boots = build_bootArray();
    }
    if (isset($_POST['showBoots'])) {
        $boots = build_bootArray(0, 1);
    }
    if (isset($_POST['showShoes'])) {
        $boots = build_bootArray(0, 2);
    }
    if (isset($_POST['search'])) {
        if ($_POST['size'] == "") {
            $_POST['size'] = 0;
        }
        //($id = 0, $type = 0, $mat = 0, $submat = 0, $heel = 0, $size = 0
        $boots = build_bootArray(0, 0, $_POST['material'], $_POST['submat'], $_POST['heel'], $_POST['size'], $_POST['height']);
    }
    if (isset($boots)) {
        $boots_table = build_bootsTable($boots);
    } else {
        $boots_table = "";
    }

}

$view1 = <<<FORM1
    <!-- Begin page content -->
    <div class="container">
        <div class="row">
                <legend>Inventory View</legend>
        </div>
        <div class="row">
                    <div class="col-md-3">
                        <!-- Left Margin -->
                    </div>
                    <div class="col-md-2 col-xs-3">
                        <form class="form-inline" action="./inventory.php" method="post">
                                <input type="hidden" name="showAll" value="1">
                                <button class="btn btn-default btn-block" type="submit">Show all</button>
                        </form>
                    </div>
                    <div class="col-md-2 col-xs-3">
                        <form class="form-inline" action="./inventory.php" method="post">
                                <input type="hidden" name="showBoots" value="1">
                                <button class="btn btn-default btn-block" type="submit">Show Boots</button>
                        </form>
                    </div>
                    <div class="col-md-2 col-xs-3">
                        <form class="form-inline" action="./inventory.php" method="post">
                                <input type="hidden" name="showShoes" value="1">
                                <button class="btn btn-default btn-block" type="submit">Show Shoes</button>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <!-- Right Margin -->
                    </div>
        </div>
        </br></br>

        <div class="row">
        <legend>Inventory Search</legend>
        <div class="col-xs-1">
        </div>
        <div class="col-xs-10">
                <form class="form-horizontal" action="./inventory.php" method="post">
                    <div class="form-group">
                <label for="inputMat" class="col-lg-3 control-label">Material</label>
                <div class="col-lg-4">
                    $MatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group" id="submatFG">
                <label for="inputSMat" class="col-lg-3 control-label">Sub-Material</label>
                <div class="col-lg-4">
                    $SMatSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group">
                <label for="inputHeel" class="col-lg-3 control-label">Heel</label>
                <div class="col-lg-4">
                    $HeelSelect
                </div>
            </div> <!-- form group -->
            <div class="form-group" id="heightSelectFG">
                <label for="inputHeight" class="col-lg-3 control-label">Height</label>
                <div class="col-lg-4">
                    $HeightSelect
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label">Size</label>
                <div class="col-lg-4">
                    <input type="text" name="size" class="form-control" placeholder="37">
                </div>
            </div>
            <div class="form-group">
                <label for="inputSize" class="col-lg-3 control-label"></label>
                <div class="col-lg-4">
                <button class="btn btn-success btn-lg btn-block" type="submit">Search</button>
                </div>
            </div>
            <input type="hidden" name="search" value="1">


                </form>
                </div>
                <div class="col-xs-1">
                </div>
        </div> <!-- /row -->
    </div> <!-- /container -->
FORM1;

if ($view == 1) {
    echo $view1;
}
if ($view == 2) {
    echo '<div class="container">';
    echo $boots_table;
    echo '</div>';
}


require 'footer_common.php';

?>
<script>

    function updateSubMats() {
        var selectMat = document.getElementById('material').value;
        if (selectMat == 1) {
            $("#submatFG").removeClass("hidden");
            $("#heightSelectFG").removeClass("hidden");
            document.getElementById('submat').value = 1;

        } else {
            $("#submatFG").addClass("hidden");

            document.getElementById('submat').value = 0;

        }

    }

    $(document).ready(function () {
        var rbBoot = document.getElementById('inputType1');
        var rbShoe = document.getElementById('inputType2');
        var selectHeel = document.getElementById('heel');
        var selectHeight = document.getElementById('height');

        rbBoot.addEventListener('change', function (e) {
            $('.boot_type_1').removeClass("hidden");
            $('.boot_type_2').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").removeClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 1
            selectHeel.value = 1;
        })

        rbShoe.addEventListener('change', function (e) {
            $('.boot_type_2').removeClass("hidden");
            $('.boot_type_1').addClass("hidden");
            $('.selectBlank').addClass("hidden");
            $("#heightSelectFG").addClass("hidden");
            $('#selectAdvice').addClass("hidden");
            selectHeight.value = 0
            selectHeel.value = 6;
        })
    })
</script>


</body>
</html>
