<?php
/**
 * Created by Charles Compton.
 * User: charles
 * Date: 6/21/14
 * Time: 5:10 PM
 */
require_once 'config.php';
require_once 'common.php';
require_once 'handle_login.php';


$PREURI = $_SERVER['REQUEST_URI'];
$URI = substr($PREURI, strlen(Config::prefix) - 1);
$active = "class='active'";
$classHome = "";
$classAdd = "";
$classInventory = "";
$classAdmin = "";
switch ($URI) {
    case "":
    case "index.php":
        $classHome = $active;
        break;

    case "add.php":
        $classAdd = $active;
        break;
    case "inventory.php":
        $classInventory = $active;
        break;
    case "adminP.php":
        $classAdmin = $active;
        break;
}


$title = Config::projectTitle;
$windowTitle = Config::windowTitle;
$cleanPrefix = Config::cleanPrefix;

if (!isset($needsLogin))
    $needsLogin = 1;
if ($needsLogin) {
    $loginnav = "<li> <a href=\"$cleanPrefix/login.php\">Login</a></li>";
} else {
    $loginnav = "<li> <a href=\"$cleanPrefix/login.php?LOGOUT=1\">Logout($username)</a></li>";
}

$adminpage = "";
if ($userIsAdmin) {
    $adminpage = "<ul class=\"nav navbar-nav pagination-lg pull-right\"><li $classAdmin >   <a href=\"$cleanPrefix/adminP.php\">Administrate</a>    </li></ul>";
}

echo <<<HERETEXT
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../favicon.ico">

    <title>$windowTitle</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="amelia.css" rel="stylesheet">




    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="$cleanPrefix/">$title</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav pagination-lg">

        <!-- NAVIGATION STUFF HERE -->
                <li $classHome >        <a href="$cleanPrefix/">Home</a>                           </li>
                <li $classAdd >         <a href="$cleanPrefix/add.php">Add Inventory</a>           </li>
                <li $classInventory >   <a href="$cleanPrefix/inventory.php">List Inventory</a>    </li>
                $loginnav
            </ul>
            $adminpage
        </div><!--/.nav-collapse -->
    </div>
</div>
</br></br></br></br>
HERETEXT;

if ($pageRequiresAdmin && !$userIsAdmin) {
    echo "<div class='container'><div class='row'><div class='alert alert-dismissable alert-warning'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <h4>Warning!</h4>
              <p>You are not allowed to view this page.";

    if ($needsLogin) {
        echo " Login and try again";
    }
    echo "</p></div></div></div>";
    require 'footer_common.php';
    die();
}

if ($pageRequiresLogin && $needsLogin) {
    echo "<div class='container'><div class='row'><div class='alert alert-dismissable alert-warning'>
              <button type='button' class='close' data-dismiss='alert'>×</button>
              <h4>Warning!</h4>
              <p>You are not allowed to view this page.  login and try again</p></div></div></div>";

    require 'footer_common.php';
    die();
}


/*
    Drop down example
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>

 */

?>