<?php
/**
 * Created by Charles Compton.
 * Purpose:  Used for handling admins.
 * User: charles
 * Date: 6/21/14
 * Time: 6:11 PM
 */
$pageRequiresAdmin = 1;
$pageRequiresLogin = 0;
$errorLogin = 0;
$adminP_warning = "";

$view = 1; // View 1 = Standard Admin view
// View 2 = Add Admin View
// View 3 = Edit Admin View
// View 4 = Delete Admin View
// View 5 = Reset Password
// View 6 = Admin Reports.
require 'header_common.php';
if ($_POST) {

    //      ADD

    if (isset($_POST['add'])) {
        $view = 2;
        $content = <<<HERECONTENT
            <form action="./adminP.php" method="post">
            <div class="col-lg-2 col-xs-1">
            </div>
            <div class="col-lg-8 col-xs-10">
                <div class="form-group row">
                    <div class="col-lg-2">
                    </div>
                    <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-4">
                        <input type="text" name="username" class="form-control" placeholder="Username" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-2">
                    </div>
                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                    <div class="col-lg-4">
                        <input type="text" name="password" class="form-control" placeholder="password" required>
                        <div class="checkbox">
                        <label>
                            <input type="checkbox" name="adminFlag"> User is Admin?
                        </label>
                    </div>
                    </div>
                </div>
                <div class="form-group row">
                    <input type="hidden" name="add2" value="1">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-6">
                    <button class="btn btn-success btn-block" type="submit">Add User</button>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-xs-1">
            </div>
            </form>
HERECONTENT;


    }
    if (isset($_POST['add2'])) {
        $mysqli = DB::cxn();
        $md5pass = md5($_POST['password']);
        $tmpUsername = strtolower($_POST['username']);
        $tmpAdminFlag = 0;
        if (isset($_POST['adminFlag'])) {
            $tmpAdminFlag = 1;
        }
        $stmt = $mysqli->prepare("insert into users (user,password,adminFlag) VALUES (?, ?, ?)");
        $stmt->bind_param('ssi', $tmpUsername, $md5pass, $tmpAdminFlag);
        $view = 1;
        if ($stmt->execute()) {

            $adminP_warning = <<<HEREWARNING
        <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>User Created</strong>
              </div>
            </div>
HEREWARNING;
        } else {

            $adminP_warning = <<<HEREWARNING
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Invalid entry, please try again.
                </div>
HEREWARNING;

            $errorLogin = 1;
        }
        $stmt->close();
    }

    //    EDIT

    if (isset($_POST['edit'])) {
        $view = 3;
        $users = build_userArray(0, $_POST['edit']);
        $user = $users[$_POST['edit']];
        $checked = ($user['admin'] ? "checked" : "");
        $tmpUsername = ucwords($user['username']);
        $content = <<<HERECONTENT
            <form action="./adminP.php" method="post">
            <div class="col-lg-2 col-xs-1">
            </div>
            <div class="col-lg-8 col-xs-10">
                <div class="form-group row">
                    <div class="col-lg-2">
                    </div>
                    <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                    <div class="col-lg-4">
                        <input type="text" name="username" class="form-control" placeholder="Username" required value='$tmpUsername'>
                        <div class="checkbox">
                        <label>
                            <input type="checkbox" name="adminFlag" $checked> User is Admin?
                        </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <input type="hidden" name="edit2" value="${user['UID']}">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-6">
                    <button class="btn btn-success btn-block" type="submit">Edit User</button>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-xs-1">
            </div>
            </form>
HERECONTENT;
    }

    if (isset($_POST['edit2'])) {
        $view = 1;
        $mysqli = DB::cxn();
        $tmpID = $_POST['edit2'];
        $tmpAdminFlag = 0;
        $tmpUsername = strtolower($_POST['username']);
        if (isset($_POST['adminFlag'])) {
            $tmpAdminFlag = 1;
        }
        $stmt = $mysqli->prepare("update users set user=?, adminFlag=? where id=?");
        $stmt->bind_param('sii', $tmpUsername, $tmpAdminFlag, $tmpID);
        if ($stmt->execute()) {
            $adminP_warning = <<<HEREWARNING
        <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>User Edited</strong>
              </div>
            </div>
HEREWARNING;
        }
    }


    //          DELETE

    if (isset($_POST['delete'])) {
        $view = 4;
        $users = build_userArray(0, $_POST['delete']);
        $user = $users[$_POST['delete']];
        $content = <<<HERECONTENT
            <div class="col-lg-2 col-xs-1">
            </div>
            <div class="col-lg-8 col-xs-10">
                <legend>Are you sure you want to delete ${user['username']}</legend>
                <div class="form-group row">
                    <div class="col-xs-2">
                    </div>
                    <div class="col-xs-4">
                        <form action="./adminP.php" method="post">
                        <input type="hidden" name="delete2" value="${user['UID']}">
                        <button class="btn btn-primary btn-block" type="submit">Yes, Delete them.</button>
                        </form>
                    </div>
                    <div class="col-xs-1">
                    </div>
                    <div class="col-xs-4">
                        <form action="./adminP.php" method="post">
                        <button class="btn btn-success btn-block" type="submit">NOPE!</button>
                        </form>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-xs-1">
            </div>
            </form>
HERECONTENT;

    }
    if (isset($_POST['delete2'])) {
        $view = 1;
        $mysqli = DB::cxn();
        $tmpID = $_POST['delete2'];
        $stmt = $mysqli->prepare("update users set active=0 where id=?");
        $stmt->bind_param('i', $tmpID);
        if ($stmt->execute()) {
            $adminP_warning = <<<HEREWARNING
        <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>User Deleted</strong>
              </div>
            </div>
HEREWARNING;
        }
    }

    //          RESET


    if (isset($_POST['reset'])) {
        $view = 5;
        $users = build_userArray(0, $_POST['reset']);
        $user = $users[$_POST['reset']];
        $tmpUsername = ucwords($user['username']);
        $content = <<<HERECONTENT
            <form action="./adminP.php" method="post">
            <div class="col-lg-2 col-xs-1">
            </div>
            <div class="col-lg-8 col-xs-10">
                <div class="form-group row">
                    <div class="col-lg-2">
                    </div>
                    <label for="inputUsername" class="col-lg-2 control-label">New Password</label>
                    <div class="col-lg-4">
                        <input type="text" name="password" class="form-control" placeholder="${tmpUsername}'s New Password" required>
                    </div>
                </div>
                <div class="form-group row">
                    <input type="hidden" name="reset2" value="${user['UID']}">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-6">
                    <button class="btn btn-success btn-block" type="submit">Edit Password</button>
                    </div>
                </div>

            </div>
            <div class="col-lg-2 col-xs-1">
            </div>
            </form>
HERECONTENT;


    }
    if (isset($_POST['reset2'])) {
        $mysqli = DB::cxn();
        $md5pass = md5($_POST['password']);
        $tmpID = $_POST['reset2'];
        $stmt = $mysqli->prepare("update users set password=? where id=?");
        $stmt->bind_param('si', $md5pass, $tmpID);
        $view = 1;
        if ($stmt->execute()) {

            $adminP_warning = <<<HEREWARNING
        <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>User Password reset</strong>
              </div>
            </div>
HEREWARNING;
        }
    }
    if (isset($_POST['report'])) {
        $view = 6;
        $interval = "";
        if (isset($_POST['rquarter'])) {
            $interval = "NOW() - INTERVAL 3 MONTH";
            $clean = "last 3 months";
        }

        if (isset($_POST['rmonthly'])) {
            $interval = "NOW() - INTERVAL 1 MONTH";
            $clean = "last 1 month";
        }

        if (isset($_POST['rweekly'])) {
            $interval = "NOW() - INTERVAL 7 DAY";
            $clean = "last 7 days";
        }
        $report = build_salesReport($interval);
        $reportdate = gmdate('m/d/Y', $report['timestamp']);
        $profit = "$" . number_format($report['retail'] - $report['wholesale'], 2);
        $totals = "$" . number_format($report['total'], 2);
        $salestax = "$" . number_format($report['salestax'], 2);
        $content = <<<HERECONTENT
         <div class = "col-md-4">
            <!-- left margin -->
        </div>
        <div class = "col-md-4">
                <legend>Sales Report for $clean</legend>
                <div class="row">
                    <label for="inputUsername" class="col-lg-8 control-label">Total Sales</label>
                    <div class="col-lg-4">
                        $totals
                    </div>
                </div>
                </br>
                <div class="row">
                    <label for="inputUsername" class="col-lg-8 control-label">Total Sales Tax</label>
                    <div class="col-lg-4">
                        $salestax
                    </div>
                </div>
                </br>
                <div class="row">
                    <label for="inputUsername" class="col-lg-8 control-label">Gross Income</br> (Not including sales tax)</label>
                    <div class="col-lg-4">
                        $profit
                    </div>
                </div>
                </br>
                <div class="row">
                    <label for="inputUsername" class="col-lg-8 control-label">Units Sold</label>
                    <div class="col-lg-4">
                        ${report['bootsold']}
                    </div>
                </div>
        </div>
        <div class = "col-md-4">
            <!-- right margin -->
        </div>
        </div> <!-- /row -->
HERECONTENT;


    }
}

$header = <<<HEREHEADER
        <div class="container">
        $adminP_warning
        <div class="row">
HEREHEADER;

$footer = <<<HEREFOOTER
    </div> <!-- /row -->
</div> <!-- /container -->
HEREFOOTER;


// NORMAL VIEW

if ($view == 1) {
    $users = build_userArray();
    $usersTable = build_userTable($users);
    $content = <<<HERECONTENT
        <div class = "col-md-4">
            <!-- left margin -->
        </div>
        <div class = "col-md-4">
            <legend>Sales reports</legend>
            <div class = "row">
                <div class="col-xs-4">
                    <form class="form-inline" action="./adminP.php" method="post">
                        <input type="hidden" name="report" value="1">
                        <input type="hidden" name="rweekly" value="1">
                        <button class="btn btn-default btn-block" type="submit">Weekly Sales</button>
                    </form>
                </div>
                <div class="col-xs-4">
                    <form class="form-inline" action="./adminP.php" method="post">
                        <input type="hidden" name="report" value="1">
                        <input type="hidden" name="rmonthly" value="1">
                        <button class="btn btn-default btn-block" type="submit">Monthly Sales</button>
                    </form>
                </div>
                <div class="col-xs-4">
                    <form class="form-inline" action="./adminP.php" method="post">
                        <input type="hidden" name="report" value="1">
                        <input type="hidden" name="rquarter" value="1">
                        <button class="btn btn-default btn-block" type="submit">Quarter Sales</button>
                    </form>
                </div>
            </div>
        </div>
        <div class = "col-md-4">
            <!-- right margin -->
        </div>
    </div> <!-- row -->
    </br></br></br>
    <div class="row">
         <div class = "col-md-4">
            <!-- left margin -->
        </div>

        <div class = "col-md-4">
                <legend>User Administration.</legend>
        $usersTable
        </div>
        <div class = "col-md-4">
            <!-- right margin -->
        </div>
        </div> <!-- /row -->
        <div class="row">
            <div class="col-md-6 col-xs-8">
            </div>
            <div class="col-md-2 col-xs-3">
                <form class="form-inline" action="./adminP.php" method="post">
                    <input type="hidden" name="add" value="1">
                    <button class="btn btn-success btn-block" type="submit">Add User</button>
                </form>
            </div>
            <div class="col-md-4 col-xs-1">
            </div>
HERECONTENT;
}

echo $header;
echo $content;
echo $footer;


require 'footer_common.php';