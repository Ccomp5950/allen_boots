<?php
/**
 * Created by Charles.
 * User: charles
 * Date: 6/21/14
 * Time: 4:59 PM
 */
$pageRequiresAdmin = 0;
$pageRequiresLogin = 0;
require 'header_common.php';
?>

<!-- Begin page content -->
<div class="container">
    <div class="page-header">
        <h3>Welcome to the Inventory Control Panel for the Exotic Stitch Boot Company.</h3>
    </div>
    <p class="lead">Use the navigation bars at the top or login to continue.</p>
</div>

<?php
require 'footer_common.php'
?>



</body>
</html>
