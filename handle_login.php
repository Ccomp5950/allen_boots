<?php
/**
 * Created by PhpStorm.
 * User: charles
 * Date: 6/21/14
 * Time: 6:11 PM
 */
if (isset($_COOKIE["session"])) {
    if (isValidMd5($_COOKIE["session"])) // Initial validation, so people don't try and do SQL injection.
    {
        $mysqli = DB::cxn();
        $stmt = $mysqli->prepare("select session.id, session.cookie, session.admin,session.user,session.timeCreated, users.id from session INNER JOIN users on users.user = session.user where session.cookie = ? limit 1 ");
        if ($stmt === false) {
            trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
            die();
        }
        $stmt->bind_param('s', $_COOKIE['session']); // We're using prepared statements though so SQL injection should be nearly impossible.
        if ($stmt === false) {
            trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
            die();
        }
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows) // Valid session so we get session information from the database.
        { //   id       cookie         admin        user        timeCreated
            $stmt->bind_result($sessionID, $sessionCookie, $userIsAdmin, $username, $sessionTimeCreated, $userID);
            $stmt->fetch();

            setcookie("session", $sessionCookie, time() + (Config::sessionValidTime * 60)); // And extend the session.
            $needsLogin = 0;
            $username = ucwords($username);
        } else {
            setcookie("session", "xxx", -1); // No valid session so we remove the cookie.
            $needsLogin = 1;
            $username = "";
        }
        $stmt->close();
    }
} else if (!isset($needsLogin)) {

    $userIsAdmin = 0;
    $needsLogin = 1;
    $username = "";

}