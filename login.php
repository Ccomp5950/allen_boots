<?php
/**
 * Created by PhpStorm.
 * Purpose:  Used for logging in and invalidating cookies if needed.
 * Special Instructions:  We can't call the header until cookies are set so we call config.php and common.php.
 * PUT:     ?logout=1 logs out deletes the cookie and removes the session from the database.
 * User: charles
 * Date: 6/21/14
 * Time: 6:11 PM
 */
$pageRequiresAdmin = 0;
$pageRequiresLogin = 0;
$errorLogin = 0;
$login_warning = "";
if (isset($_GET["LOGOUT"])) {
    setcookie("session", "xxx", -1);
    //Do logout stuffs
    $login_warning = <<<HEREWARNING
         <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>You have been logged out</strong>
              </div>
            </div>
HEREWARNING;
    $errorLogin = 1;
    $oldcookie = $_COOKIE['session'];
    unset($_COOKIE['session']);
    require_once 'config.php';
    require_once 'common.php';
    $mysqli = DB::cxn();
    $stmt = $mysqli->prepare("DELETE FROM session WHERE cookie=?");
    $stmt->bind_param("s", $oldcookie);
    $stmt->execute();
    $stmt->close();

} elseif ($_POST) {
    require_once 'config.php';
    require_once 'common.php';

    if (isset($_POST['username'])) {
        $mysqli = DB::cxn();
        $md5pass = md5($_POST['password']);
        $tmpUsername = strtolower($_POST['username']);
        $stmt = $mysqli->prepare("select user,adminFlag from users where user = ? and password = ? and active = 1");
        $stmt->bind_param('ss', $tmpUsername, $md5pass);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows) {
            $session = md5("Something something 438418r21" . time());
            setcookie("session", $session, time() + (Config::sessionValidTime * 60));
            $stmt->bind_result($username, $userIsAdmin);
            $stmt->fetch();
            //id,cookie,admin,user,timeCreated

            $insert = $mysqli->prepare("INSERT INTO session (cookie,admin,user,timeCreated) values (?, ?, ?, now())");
            $insert->bind_param('sis', $session, $userIsAdmin, $username);
            $insert->execute();
            $insert->close();
            $login_warning = <<<HEREWARNING
        <div class="bs-component">
              <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>You have been logged in.</strong>
              </div>
            </div>
HEREWARNING;
            $errorLogin = 1;
            header("location: " . Config::cleanPrefix . "/");


        } else {

            $tmpPassword = $_POST['password'];
            $login_warning = <<<HEREWARNING
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Invalid Login, please try again.
                </div>
HEREWARNING;

            $errorLogin = 1;
        }
        $stmt->close();
    }
} else {
    require_once 'config.php';
    require_once 'common.php';
}

require 'header_common.php';

$header = '<div class="container">';
$login = <<<HERETEXT
    <div class="row">
      <div class="col-md-4 col-xs-1">
      </div>
      <div class="col-md-4 col-xs-10">
      $login_warning
      <form class="form-horizontal" action="./login.php" method="post">
                <fieldset>
                  <legend>Please sign in</legend>
         Username<br>
        <input type="text" name="username" class="form-control" placeholder="username" required autofocus><br>
        Password<br>
        <input type="password" name="password" class="form-control" placeholder="Password" required>
        </br></br>
        <button class="btn btn-success btn-lg btn-block" type="submit">Sign in</button>
        </fieldset>
      </form>
      </div>
      <div class="col-md-4 col-xs-1">
      </div>
    </div> <!-- /row -->
    </div> <!-- /container -->
HERETEXT;

echo "$header\n";
if ($needsLogin) {
    echo "$login";
} else {
    echo "$login_warning";
}

echo "<br><br><br><br>";
require 'footer_common.php';