<?php
/**
 * Created by Charles.
 * User: charles
 * Date: 6/26/14
 * Time: 2:28 PM
 */

$pageRequiresAdmin = 0;
$pageRequiresLogin = 1;
$add_message = "";
require_once 'config.php';
require_once 'common.php';
require_once 'handle_login.php';
$ArrayMat = build_materialArray();
$ArraySMat = build_submatArray();
$ArrayHeel = build_heelArray();
$ArrayHeight = build_heightArray();
if ($_POST) {
    if (isset($_POST['sell'])) {
        $boots = build_bootArray($_POST['sell']);
        $boot = $boots[$_POST['sell']];
    }
    if (isset($_POST['sell2'])) {
        list($add_message, $error) = check_sell();
        if (!$error) {
            $mysqli = DB::cxn();
            $query = "update boots set retailPrice=?, status=2, salestax=?, totalsale = ?, dateSold=now() where id=?;";
            $stmt = $mysqli->prepare($query);

            $stmt->bind_param("dddi", $_POST['retail'], $_POST['tax'], $_POST['totalsale'], $_POST['sell2']);
            if ($stmt->execute()) {
                header("location: " . Config::cleanPrefix . "/inventory.php?showAll=1");
                die();
            } else {
                trigger_error('Wrong SQL: ' . $query . ' Error: ' . $mysqli->errno . ' ' . $mysqli->error, E_USER_ERROR);
                die();
            }

        }
    }


} else {
    header("location: " . Config::cleanPrefix . "/");
    die;
}
$MatSelect = build_selectOptions($ArrayMat, "material", $boot['mat'], 0, 0, "onchange='updateSubMats()'");
$SMatSelect = build_selectOptions($ArraySMat, "submat", $boot['smat'], 1, 0);
$HeelSelect = build_selectOptions($ArrayHeel, "heel", $boot['heel'], 0, 1, $boot['type']);
$HeightSelect = build_selectOptions($ArrayHeight, "height", $boot['height'], 1);

require 'header_common.php';

$smathidden = "";
if (is_null($boot['smat']) || $boot['smat'] == 0)
    $smathidden = " hidden";
$bootselected = "";
$shoeselected = "";

if ($boot['type'] == 1) {
    $bootselected = "checked";
} else {
    $shoeselected = "checked";
}

$describe_boot = "Size ${boot['size']}, " . $boot['color'] . ", " . ucwords($ArrayMat[$boot['mat']]['material']);
if ($boot['type'] == 1) {
    $describe_boot .= " Boots";
} else {
    $describe_boot .= " Shoes";
}

$describe_boot .= " (InvID: ${boot['InvID']})";
$sales_tax = "0.00";
$total = $boot['RPrice'];
if (Config::chargeSalesTax) {
    $sales_tax = round($boot['RPrice'] * Config::salesTax, 2);
    $salesTaxChecked = "checked";
    $total = $boot['RPrice'] + $sales_tax;
}

echo <<<HERETEXT


<div class="container">
    <div class="row">
        $add_message
        <h2>Sell inventory</h2>
    </div>
        <form class="form-horizontal" action="./sell.php" method="post">
    <div class="row">
            <table class="table table-striped">
            <thead>
                <tr class = "td-lg">
                    <th>Item</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            <tr class = "td-lg">
                <td class="invoice-item">$describe_boot</br>${boot['desc']}</td>
                <td class="invoice-price"><input type="text" onchange='updateSalesTax()' name="retail" class="form-control" placeholder="240.00" id="rprice" required value='${boot['RPrice']}'>
            </tr>
            <tr>
                <td class="invoice-item" align="right">
                    <label>
                        <input type="checkbox" id="salestaxcheck" $salesTaxChecked onchange='updateSTCB()'> Sales Taxes
                    </label>
            </td>
                <td class="invoice-price"><input type="text" name="tax" class="form-control" placeholder="240.00" id="salestaxbox" required value='$sales_tax' readonly>
                </td>
            </tr>
            <tr>
                <td class="invoice-item" align = "right">Total:&nbsp;</td>
                <td class="invoice-price"><input type="text" class="form-control" name="totalsale" id="pricetotal" required value='$total' readonly>
            </tr>
            </table>

    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
        <input type='hidden' name='sell2' value='${boot['ID']}'>
        </br>
        <button class="btn btn-success btn-md btn-block" type="submit">SELL BOOT</button>
        </div>
        <div class="col-lg-2">
        <!-- Right -->
    </form>
    </div> <!-- /row -->
</div> <!-- /container -->

HERETEXT;


require 'footer_common.php'
?>

<!-- Start hide stuffs script -->
<script>

    function updateSTCB() {
        updateSalesTax();
    }

    function updateSalesTax() {
        var rprice = document.getElementById('rprice');
        var retail = parseFloat(rprice.value);
        var salestax = document.getElementById('salestaxbox');
        var salestaxcheck = document.getElementById('salestaxcheck');
        var totalbox = document.getElementById('pricetotal');
        var newsalestax = 0;
        if (salestaxcheck.checked) {
            newsalestax = retail * <?php echo Config::salesTax ?>;
        }
        rprice.value = parseFloat(retail).toFixed(2);
        salestax.value = parseFloat(newsalestax).toFixed(2);
        var totalprice = parseFloat(newsalestax) + parseFloat(retail) + .001;
        totalbox.value = parseFloat(totalprice).toFixed(2);
    }
</script>
</body>
</html>
